var gulp = require('gulp');
var del = require('del');
var less = require('gulp-less');
var path = require('path');
var watch = require('gulp-watch');

gulp.task('clear', function () {
    return del(['./css/style.css']);
});

gulp.task('less', ['clear'], function () {
    return gulp.src('css/less/*.less')
        .pipe(less())
        .pipe(gulp.dest('css/'));
});

gulp.task('watch', ['less'], function () {
    watch(['./css/less/*'], function () {
        gulp.start('less');
    });
});