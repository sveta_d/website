$(function() {

    $("nav ul li").click(function () {
        $("header ul li").removeClass("active");
        $(this).addClass("active");
    });

    $("nav li a[href*='#']").mPageScroll2id();

    $( '.swipe' ).swipebox();
});